## [1.3.11](https://gitlab.com/biletado/frontend/angular/compare/v1.3.10...v1.3.11) (2025-01-20)


### Bug Fixes

* **deps:** update angular with cli monorepo ([3c1bac3](https://gitlab.com/biletado/frontend/angular/commit/3c1bac3f67c4231e58a876b37f4bc5e536846e64))

## [1.3.10](https://gitlab.com/biletado/frontend/angular/compare/v1.3.9...v1.3.10) (2025-01-20)


### Bug Fixes

* **deps:** update angular with cli monorepo to v19.1.2 ([bdee2f8](https://gitlab.com/biletado/frontend/angular/commit/bdee2f8d7000df2b7d568dbb658d24aa73026fb9))

## [1.3.9](https://gitlab.com/biletado/frontend/angular/compare/v1.3.8...v1.3.9) (2025-01-16)


### Bug Fixes

* **deps:** update angular with cli monorepo to v19.1.1 ([9ad526a](https://gitlab.com/biletado/frontend/angular/commit/9ad526a3b8201fede79e07ef6ddda0d0cec01b88))

## [1.3.8](https://gitlab.com/biletado/frontend/angular/compare/v1.3.7...v1.3.8) (2025-01-15)


### Bug Fixes

* **deps:** update angular with cli monorepo to v19.1.0 ([62d2e4a](https://gitlab.com/biletado/frontend/angular/commit/62d2e4a7ecac858454e715bb04ec7f03b3303c61))

## [1.3.7](https://gitlab.com/biletado/frontend/angular/compare/v1.3.6...v1.3.7) (2025-01-12)


### Bug Fixes

* **deps:** update dependency @ng-bootstrap/ng-bootstrap to v18 ([ce21560](https://gitlab.com/biletado/frontend/angular/commit/ce2156082e02dd633a9e3a09ed43af02aade792d))

## [1.3.6](https://gitlab.com/biletado/frontend/angular/compare/v1.3.5...v1.3.6) (2025-01-12)


### Bug Fixes

* **deps:** update dependency keycloak-angular to v19 ([2929367](https://gitlab.com/biletado/frontend/angular/commit/29293675d049ed8b50ccfa4e6ade15200e1b96ee))

## [1.3.5](https://gitlab.com/biletado/frontend/angular/compare/v1.3.4...v1.3.5) (2025-01-12)


### Bug Fixes

* **CI:** fix semrel commit-message ([115e609](https://gitlab.com/biletado/frontend/angular/commit/115e609c31ffecd3bf98070730843c98d3a126b5))

## [1.3.4](https://gitlab.com/biletado/frontend/angular/compare/v1.3.3...v1.3.4) (2025-01-12)


### Bug Fixes

* **CI:** configure semrel commit-message and disable gitlab-plugin ([9a13335](https://gitlab.com/biletado/frontend/angular/commit/9a13335a70543b3b05c40dcded88b2be51b42ad9))

## [1.3.3](https://gitlab.com/biletado/frontend/angular/compare/v1.3.2...v1.3.3) (2025-01-12)


### Bug Fixes

* **CI:** use .releaserc instead of release.config ([413f26d](https://gitlab.com/biletado/frontend/angular/commit/413f26d43b0374c2aedbb5edfbc9186fa47b1189))
