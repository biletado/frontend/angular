FROM docker.io/library/node:22-alpine AS base

FROM base AS package
WORKDIR /app
COPY package.json ./package.orig.json
COPY yarn.lock ./
RUN node -e "const fs = require('fs'); const json = JSON.parse(fs.readFileSync('./package.orig.json')); delete json.version; fs.writeFileSync('./package.json', JSON.stringify(json, null, 2));"

# Install dependencies only when needed
FROM base AS deps
WORKDIR /app

# Install dependencies based on the preferred package manager
COPY --from=package /app/package.json /app/yarn.lock ./
RUN yarn install

# Rebuild the source code only when needed
FROM base AS builder
WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY ./ ./
RUN yarn build

FROM docker.io/library/nginx:1.27-alpine
COPY --from=builder /app/dist/biletado /usr/share/nginx/html
EXPOSE 80
