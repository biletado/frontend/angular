import { InjectionToken } from '@angular/core';
import { KeycloakConfig } from "keycloak-js";

export interface AppConfig {
  /**
   * contains a absolute or relative URI to the api-endpoint
   */
  apiUrl: string
  keycloak: KeycloakConfig
  dashboardUrls?: {[index: string]: string}
  statusUrls?: {[index: string]: string}
}

export let APP_CONFIG = new InjectionToken<AppConfig>('APP_CONFIG')
