import { NgModule, inject, provideAppInitializer } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APP_CONFIG, AppConfig } from "../app.config";

function initializeKeycloak(keycloak: KeycloakService, config: AppConfig) {
  return () =>
    keycloak.init({
      config: config.keycloak,
      initOptions: {
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri:
          window.location.origin + '/assets/silent-check-sso.html'
      }
    });
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    KeycloakAngularModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [
    provideAppInitializer(() => {
        const initializerFn = (initializeKeycloak)(inject(KeycloakService), inject(APP_CONFIG));
        return initializerFn();
      })
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
