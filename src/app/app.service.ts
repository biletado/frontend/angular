import { Inject, Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { APP_CONFIG, AppConfig } from "../app.config";

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private appConfig: AppConfig) { }

  getJWTInfo() {
    return this.http.get(this.appConfig.apiUrl, {withCredentials: true});
  }
}
