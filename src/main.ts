import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { APP_CONFIG } from './app.config';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

fetch('/assets/config.json', {cache: "reload"})
  .then((response) => response.json())
  .then((config) => {
    if (environment.production) {
      enableProdMode();
    }

    const extraProviders = [{ provide: APP_CONFIG, useValue: config }]
    platformBrowserDynamic(extraProviders).bootstrapModule(AppModule)
      .catch(err => console.error(err));
  })
